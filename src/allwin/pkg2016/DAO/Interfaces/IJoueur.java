/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package allwin.pkg2016.DAO.Interfaces;

import allwin.pkg2016.Entities.Joueur;
import allwin.pkg2016.Entities.Match;

/**
 *
 * @author Bilel
 */
public interface IJoueur extends IDao<Joueur, Integer>{
    public void create(Joueur M);
    public void delete(Joueur M);
    public Joueur find(int id);
    public String find(String nom);
    public Joueur findByLicense(int license);
}
