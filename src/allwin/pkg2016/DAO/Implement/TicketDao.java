/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package allwin.pkg2016.DAO.Implement;

import allwin.pkg2016.DAO.Interfaces.*;
import allwin.pkg2016.Entities.*;

import allwin.pkg2016.DataSource.DataSource;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import java.util.Date;

/**
 *
 * @author WiKi
 */
public class TicketDao implements ITicketDao {

    @Override
    public void ajouterTicket(Ticket e) {

        try {
            String req1 = "insert into ticket (idTicket,prixTicket,idMatch,idMembre,chaise) values (?,?,?,?,?,?)";
            String req2 = "select count(*) from ticket  where idMatch = '" + e.getMatch().getIdMatch() + "'";

            PreparedStatement ps = DataSource.getDataSource().getConnection().prepareStatement(req1);
            PreparedStatement ps1 = DataSource.getDataSource().getConnection().prepareStatement(req2);
            ResultSet res1 = ps1.executeQuery();
            int s = res1.getInt(1);
            if (e.getMatch().getNbrTicket() > s) {
                ps.setInt(1, e.getIdTicket());

                ps.setFloat(2, e.getPrix());
                ps.setInt(3, e.getMatch().getIdMatch());
                ps.setInt(4, e.getFan().getId());
                ps.setInt(5, e.getChaise());
                ps.executeUpdate();
            } else {
                System.out.println("impossible d'ajouter le tiquet ");
            }
        } catch (SQLException ex) {
            Logger.getLogger(TicketDao.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Override
    public void supprimerTicket(Ticket e) {

        try {
            String req1 = "DELETE FROM Ticket WHERE idTicket = '" + e.getIdTicket() + "'";
            PreparedStatement ps = DataSource.getDataSource().getConnection().prepareStatement(req1);
            ps.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(TicketDao.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Override
    public void payerTicket(Fan f, Ticket e) {
        if (f.getCredit() < e.getPrix()) {
            System.out.println("credit insuffisant");

        } else {
            ajouterTicket(e);
            f.setCredit(f.getCredit() - e.getPrix());

        }
    }

}
//}
