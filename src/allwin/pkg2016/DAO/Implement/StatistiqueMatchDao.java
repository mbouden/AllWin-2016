/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package allwin.pkg2016.DAO.Implement;

import allwin.pkg2016.DAO.Interfaces.IStatistiqueMatch;
import allwin.pkg2016.DataSource.DataSource;
import allwin.pkg2016.Entities.StatistiquesMatch;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author bilel
 */
public class StatistiqueMatchDao implements IStatistiqueMatch{
        public void create(StatistiquesMatch sm){
        
        
        
        String req = "INSERT INTO statsmatch (idMatch,idMembre,nbrAces,nbrDoubleFaults,pPremierService,ptsPremierService,ptsDeuxiemeService,balleBreak,ptsSurRetour,totalPtsGagnes) values(?,?,?,?,?,?,?,?,?,?)";
        try {
            PreparedStatement pr = DataSource.getDataSource().getConnection().prepareStatement(req);

            pr.setInt(1, sm.getMatch().getIdMatch());
            pr.setInt(2, sm.getJoueur().getId());
            pr.setInt(3, sm.getNbrAces());
            pr.setInt(4, sm.getNbrDoubleFaults());
            pr.setFloat(5, sm.getpPremierService());
            pr.setInt(6, sm.getPtsPremierService());
            pr.setInt(7, sm.getPtsDeuxiemeService());
            pr.setInt(8, sm.getBalleBreak());
            pr.setInt(9, sm.getPtsSurRetour());
            pr.setInt(10, sm.getTotalPtsGagnes());
            pr.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(ArbitreDao.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Override
    public void update(StatistiquesMatch sm) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(Integer ids) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public StatistiquesMatch find(Integer ids) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
        

}
