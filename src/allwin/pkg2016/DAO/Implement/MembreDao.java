/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package allwin.pkg2016.DAO.Implement;

import allwin.pkg2016.DAO.Interfaces.IMembre;
import allwin.pkg2016.DataSource.DataSource;
import allwin.pkg2016.Entities.Compte;
import allwin.pkg2016.Entities.Fan;
import allwin.pkg2016.Entities.Joueur;
import allwin.pkg2016.Entities.Administrateur;
import allwin.pkg2016.Entities.ResponsableAntiDopage;
import allwin.pkg2016.Entities.Medecin;
import allwin.pkg2016.Entities.Arbitre;
import allwin.pkg2016.Entities.Membre;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Mahmoud
 */
public class MembreDao implements IMembre {

    @Override
    public Membre Authentification(String login, String password) {
        String requete = "select * from membre where userNameMembre=? and passwordMembre=? ";
        System.out.println(login + "  " + password);
        try {
            PreparedStatement ps = DataSource.getDataSource().getConnection().prepareStatement(requete);
            ps.setString(1, login);
            ps.setString(2, password);
            ResultSet resultat = ps.executeQuery();
            Membre a = new Membre();
            Compte c = new Compte();

            while (resultat.next()) {
                switch (resultat.getString("type")) {
                    case "Fan":
                    {
                        Fan b = new Fan();
                        b.setId(resultat.getInt("idMembre"));
                        b.setNom(resultat.getString("nomMembre"));
                        b.setPrenom(resultat.getString("prenomMembre"));
                        b.setSexe(resultat.getString("sexeMembre"));
                        b.setNumTel(resultat.getInt("numTelMembre"));
                        b.setDateNaissance(resultat.getDate("dateNaissanceMembre"));
                        c.setMail(resultat.getString("emailMembre"));
                        c.setLogin(resultat.getString("userNameMembre"));
                        c.setPasseword(resultat.getString("passwordMembre"));
                        b.setCompte(c);
                        System.out.println(c.getLogin() + "  " + c.getPasseword());
                        System.out.println("Fan");
                        return b;
                    }
                    case "Joueur":
                    {
                        Joueur b = new Joueur();
                        b.setId(resultat.getInt("idMembre"));
                        b.setNom(resultat.getString("nomMembre"));
                        b.setPrenom(resultat.getString("prenomMembre"));
                        b.setSexe(resultat.getString("sexeMembre"));
                        b.setNumTel(resultat.getInt("numTelMembre"));
                        b.setDateNaissance(resultat.getDate("dateNaissanceMembre"));
                        b.setNumLicense(resultat.getInt("numLicenceMembre"));
                        b.setClassement(resultat.getInt("classement"));
                        b.setDateLicense(resultat.getDate("dateCreationLicence"));
                        b.setScore(resultat.getInt("score"));
                        b.setPays(resultat.getString("pays"));
                        c.setMail(resultat.getString("emailMembre"));
                        c.setLogin(resultat.getString("userNameMembre"));
                        c.setPasseword(resultat.getString("passwordMembre"));
                        b.setCompte(c);
                        System.out.println(c.getLogin() + "  " + c.getPasseword());
                        System.out.println("Joueur");
                        return b;
                    }
                    case "Admin":
                    {
                        Administrateur b = new Administrateur();
                        b.setId(resultat.getInt("idMembre"));
                        b.setNom(resultat.getString("nomMembre"));
                        b.setPrenom(resultat.getString("prenomMembre"));
                        b.setSexe(resultat.getString("sexeMembre"));
                        b.setNumTel(resultat.getInt("numTelMembre"));
                        b.setDateNaissance(resultat.getDate("dateNaissanceMembre"));
                        c.setMail(resultat.getString("emailMembre"));
                        c.setLogin(resultat.getString("userNameMembre"));
                        c.setPasseword(resultat.getString("passwordMembre"));
                        b.setCompte(c);
                        System.out.println(c.getLogin() + "  " + c.getPasseword());
                        System.out.println("Joueur");
                        return b;
                    }
                    case "Arbitre":
                    {
                        Arbitre b = new Arbitre();
                        b.setId(resultat.getInt("idMembre"));
                        b.setNom(resultat.getString("nomMembre"));
                        b.setPrenom(resultat.getString("prenomMembre"));
                        b.setSexe(resultat.getString("sexeMembre"));
                        b.setNumTel(resultat.getInt("numTelMembre"));
                        b.setDateNaissance(resultat.getDate("dateNaissanceMembre"));
                        b.setCin(resultat.getInt("cinMembre"));
                        b.setNumService(resultat.getInt("numServiceMembre"));
                        c.setMail(resultat.getString("emailMembre"));
                        c.setLogin(resultat.getString("userNameMembre"));
                        c.setPasseword(resultat.getString("passwordMembre"));
                        b.setCompte(c);
                        System.out.println(c.getLogin() + "  " + c.getPasseword());
                        System.out.println("Joueur");
                        return b;
                    }
                    case "Medecin":
                    {
                        Medecin b = new Medecin();
                        b.setId(resultat.getInt("idMembre"));
                        b.setNom(resultat.getString("nomMembre"));
                        b.setPrenom(resultat.getString("prenomMembre"));
                        b.setSexe(resultat.getString("sexeMembre"));
                        b.setNumTel(resultat.getInt("numTelMembre"));
                        b.setDateNaissance(resultat.getDate("dateNaissanceMembre"));
                        b.setCin(resultat.getInt("cinMembre"));
                        b.setNumService(resultat.getInt("numServiceMembre"));
                        c.setMail(resultat.getString("emailMembre"));
                        c.setLogin(resultat.getString("userNameMembre"));
                        c.setPasseword(resultat.getString("passwordMembre"));
                        b.setCompte(c);
                        System.out.println(c.getLogin() + "  " + c.getPasseword());
                        System.out.println("Joueur");
                        return b;
                    }
                    case "Responsable":
                    {
                        ResponsableAntiDopage b = new ResponsableAntiDopage();
                        b.setId(resultat.getInt("idMembre"));
                        b.setNom(resultat.getString("nomMembre"));
                        b.setPrenom(resultat.getString("prenomMembre"));
                        b.setSexe(resultat.getString("sexeMembre"));
                        b.setNumTel(resultat.getInt("numTelMembre"));
                        b.setDateNaissance(resultat.getDate("dateNaissanceMembre"));
                        b.setCin(resultat.getInt("cinMembre"));
                        b.setNumService(resultat.getInt("numServiceMembre"));    
                        c.setMail(resultat.getString("emailMembre"));
                        c.setLogin(resultat.getString("userNameMembre"));
                        c.setPasseword(resultat.getString("passwordMembre"));
                        b.setCompte(c);
                        System.out.println(c.getLogin() + "  " + c.getPasseword());
                        System.out.println("Joueur");
                        return b;
                    }
                    default:
                        break;
                }
            }
            return null;
        } catch (SQLException ex) {
            System.out.println("erreur lors du chargement " + ex.getMessage());
            return null;
        }
    }

    @Override
    public List<Membre> DisplayAllFan() {

        String requete = "select * from membre where type!='Admin'";
        List<Membre> fans = new ArrayList<Membre>();
        try {
            PreparedStatement ps = DataSource.getDataSource().getConnection().prepareStatement(requete);
            ResultSet resultat = ps.executeQuery();
            while (resultat.next()) {
                Membre a = new Membre();
                Compte c = new Compte();
                a.setNom(resultat.getString("nomMembre"));
                a.setPrenom(resultat.getString("prenomMembre"));
                a.setDateNaissance(resultat.getDate("dateNaissanceMembre"));
                a.setSexe(resultat.getString("sexeMembre"));
                a.setNumTel(resultat.getInt("numTelMembre"));
                c.setMail(resultat.getString("emailMembre"));
                c.setLogin(resultat.getString("usernameMembre"));
                c.setPasseword(resultat.getString("passwordMembre")); 
                a.setCompte(c);
                fans.add(a);
            }
            return fans;
        } catch (SQLException ex) {
            System.out.println("erreur lors du chargement" + ex.getMessage());
            return null;
        }

    }
    @Override
    public void DesactivateUtilisateur(int num) {
        String requete = "update Membre set usernameMembre=? where numTelMembre=?";
        try {
            PreparedStatement ps = DataSource.getDataSource().getConnection().prepareStatement(requete);
            ps.setString(1, "fklddfdsqsgfsdssddfdkj");
            ps.setInt(2,num);
            ps.executeUpdate();
            System.out.println("Compte est désactivé");
        } catch (SQLException ex) {
            System.out.println("erreur"+ ex.getMessage());
        }
    }
}
