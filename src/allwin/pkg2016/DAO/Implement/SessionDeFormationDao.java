/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package allwin.pkg2016.DAO.Implement;

import allwin.pkg2016.DAO.Interfaces.*;
import allwin.pkg2016.Entities.*;

import allwin.pkg2016.DataSource.DataSource;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author WiKi
 */

public class SessionDeFormationDao implements ISessionDeFormation {

    @Override
    public void ajoutSession(SessionDeFormation e) {
        try {
            String req1 = "insert into sessionformation  (idSessionFormation,dateDebutFormation,dateFinFormation,NbrCandidat,prixFormation,lieuFormation) values (?,?,?,?,?,?)";
            PreparedStatement ps = DataSource.getDataSource().getConnection().prepareStatement(req1);
            ps.setInt(1, e.getIdFormation());
            java.util.Date dt = new java.util.Date();

            java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd");
            ps.setString(2, sdf.format(e.getDateDebut()));
            ps.setString(3, sdf.format(e.getDateFin()));
            ps.setInt(4, e.getNbrCandidats());
            ps.setFloat(5, e.getPrix());
            ps.setString(6, e.getLieu());

            ps.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(TicketDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void supprimerSession(SessionDeFormation e) {
        try {
            String req1 = "DELETE FROM sessionformation WHERE idSessionFormation = '" + e.getIdFormation() + "'";
            PreparedStatement ps = DataSource.getDataSource().getConnection().prepareStatement(req1);
            ps.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(TicketDao.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
