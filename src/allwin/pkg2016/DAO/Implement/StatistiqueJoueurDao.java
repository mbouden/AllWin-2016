/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package allwin.pkg2016.DAO.Implement;
 import allwin.pkg2016.DAO.Interfaces.*;
 import  allwin.pkg2016.Entities.*;

import allwin.pkg2016.DataSource.DataSource ;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author WiKi
 */
public class StatistiqueJoueurDao implements IStatistiqueJoueur {
    @Override
    public void  ajouterStatistique(StatistiquesJoueur e){
        
         try{ String req1 = "insert into statsjoueurs  (idStatJoueur,idMembre,tailleJoueur,PoidsJoueur,debutCarriere,mainPrefere,primeJoueur,nbrTitre,paysJoueur) values (?,?,?,?,?,?,?,?,?)";
            PreparedStatement ps = DataSource.getDataSource().getConnection().prepareStatement(req1);
    ps.setInt(1, e.getIdStatistiques());
    
 ps.setInt(2, e.getJoueur().getCin());
  ps.setFloat(3, e.getTaille());
   ps.setFloat(4, e.getPoids());
   java.util.Date dt = new java.util.Date();

      java.text.SimpleDateFormat sdf = 
      new java.text.SimpleDateFormat("yyyy-MM-dd");
    ps.setString(5, sdf.format(e.getDebutDeCarriere()));
     ps.setString(6, e.getMainPrefere());
     ps.setFloat(7,e.getPrime());
     ps.setInt(8, e.getNombreDeTitre());
     ps.setString(9, e.getPays());
     
     
              ps.executeUpdate();
    
       }
      catch (SQLException ex) {
            Logger.getLogger(TicketDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    @Override
   public void supprimerStatistique(StatistiquesJoueur e){   
       try {  
       String req1 =  "DELETE FROM statsjoueurs WHERE idStatJoueur = '"+e.getIdStatistiques()+"'" ;
            PreparedStatement ps = DataSource.getDataSource().getConnection().prepareStatement(req1);
             ps.executeUpdate();
            
        
    }
        
catch (SQLException ex) {
            Logger.getLogger(TicketDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    
    }
    
}
