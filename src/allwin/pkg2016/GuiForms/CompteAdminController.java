/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package allwin.pkg2016.GuiForms;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Mahmoud
 */
public class CompteAdminController implements Initializable {
    Main application;
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    public void setApp(Main application) {
        this.application = application;
    }
    @FXML    
    public void clickBack () {
        application.gotoAllWin();
    }
    @FXML    
    public void clickAjoutJoueur () {
        application.gotoAjoutJoueur();
    }
    @FXML    
    public void clickBannir () {
        application.gotoListeMembres();
    }
    public void clickCompetition () {
        application.gotoListeComeptition();
    }

    @FXML
    private void clickAjoutPersonnel(ActionEvent event) {
        application.gotoAjoutPersonnel();
    }
    
    @FXML
    private void clickClub(ActionEvent event) {
        application.gotoClub();
    }
}
