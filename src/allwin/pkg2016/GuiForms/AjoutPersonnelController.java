/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package allwin.pkg2016.GuiForms;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.ContextMenuEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Window;

/**
 * FXML Controller class
 *
 * @author Mahmoud
 */
public class AjoutPersonnelController extends AnchorPane implements Initializable {
    Main application;
    @FXML
    ChoiceBox ComboBoxSexe;
    @FXML
    ChoiceBox ComboBoxProfession;
    
    ObservableList<String> comboSexList = FXCollections.observableArrayList("Homme","Femme");
    
    ObservableList<String> comboProfessionList = FXCollections.observableArrayList("Arbitre","Médecin","Responsable Anti-Dopage");
    @FXML
    private TextField NomTF;
    @FXML
    private TextField PrenomTF;
    @FXML
    private TextField EmailTF;
    @FXML
    private TextField CinTF;
    @FXML
    private TextField NumServiceTF;
    @FXML
    private DatePicker DateNaissance;
    @FXML
    private Button ButtonImage;
    @FXML
    private Button ButtonAjouter;
    @FXML
    private Button ButtonBack;
    @FXML
    private Text text;
    @FXML
    private ImageView ImageviewProfile;
    @FXML
    private TextField CritereTF;
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        NumServiceTF.setVisible(false);
        text.setVisible(false);
        ComboBoxSexe.setValue("Homme");
        ComboBoxSexe.setItems(comboSexList);
        
        ComboBoxProfession.setValue("Arbitre");
        ComboBoxProfession.setItems(comboProfessionList);
    }    
    
    public void setApp (Main application) {
        this.application=application;
    }
    
    @FXML
    public void clickBack () {
        application.gotoCompteAdmin();
    }

    @FXML
    private void clickImage(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Resource File");
        Window stage = null;
        File file = fileChooser.showOpenDialog(stage);

        if (file != null) {

            Image image = new Image(file.toURI().toString());
            ImageviewProfile.setImage(image);
        }
    }

    @FXML
    private void clickAjouter(ActionEvent event) {
    }

    private void selectionChange(ActionEvent event) {
        if (ComboBoxProfession.getSelectionModel().getSelectedItem().equals("Arbitre")) {
            text.setText("Degré :");
        }
        else if (ComboBoxProfession.getSelectionModel().getSelectedItem().equals("Médecin")) {
            text.setText("Spécialité :");
        }
        else if (ComboBoxProfession.getSelectionModel().getSelectedItem().equals("Responsable Anti-Dopage")) {
            text.setText("Département :");
        }
        NumServiceTF.setVisible(true);
        text.setVisible(true);
    }

    @FXML
    private void selectionChange(MouseEvent event) {
    }
}
