/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package allwin.pkg2016.GuiForms;

import allwin.pkg2016.DAO.Implement.CompteRenduDao;
import allwin.pkg2016.DAO.Implement.JoueurDao;
import allwin.pkg2016.DAO.Interfaces.ICompteRendu;
import allwin.pkg2016.DAO.Interfaces.IJoueur;
import allwin.pkg2016.Entities.CompteRendu;
import allwin.pkg2016.Entities.Joueur;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author Mahmoud
 */
public class CompteRenduController extends AnchorPane implements Initializable {
    Main applictaion;
    
    @FXML
    TextField LicenseTF;
    @FXML
    DatePicker date;
    @FXML
    TextField EchantTF;
    @FXML
    TextField PHTF;
    @FXML
    TextField DensiteTF;
    @FXML
    TextField VolumeTF;
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
    @FXML
    public void setApp(Main application) {
        this.applictaion= application;
    }
    
    @FXML
    public void clickEnvoyer() {
        int license = Integer.valueOf(LicenseTF.getText());
//        LocalDate d = date.getValue();
        int ech = Integer.valueOf(EchantTF.getText());
        int ph = Integer.valueOf(PHTF.getText());
        int dens = Integer.valueOf(DensiteTF.getText());
        int vol = Integer.valueOf(VolumeTF.getText());
        
        
        IJoueur J = new JoueurDao();
        Joueur j = new Joueur();
        j= J.findByLicense(license);
        
        ICompteRendu CR = new CompteRenduDao();
        CompteRendu cr = new CompteRendu (j,ech,ph,dens,vol,java.sql.Date.valueOf("2016-10-20"));
        
        CR.create(cr);
    }
    
    public void clickBack () {
        applictaion.gotoCompteMedecin();
    }
}
