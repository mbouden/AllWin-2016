/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package allwin.pkg2016.GuiForms;

import allwin.pkg2016.Entities.Administrateur;
import allwin.pkg2016.Entities.Arbitre;
import allwin.pkg2016.Entities.Fan;
import allwin.pkg2016.Entities.Medecin;
import allwin.pkg2016.Entities.Joueur;
import allwin.pkg2016.Entities.ResponsableAntiDopage;
import allwin.pkg2016.Entities.Membre;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;

/**
 * FXML Controller class
 *
 * @author Mahmoud
 */
public class AllWinController extends AnchorPane implements Initializable {
    Main application;
    @FXML private Text Textflow;
    @FXML
    private TableColumn<?, ?> TableResultMatch;
    @FXML
    private Tab PariTab;
    @FXML
    private ListView<?> ListviewPari;
    @FXML
    private Button ButtonMonCompte;
    
    @FXML
    public void setApp(Main application) {
        this.application = application;
        if (application.getMembre()==null) {
            String msg="Bienvenue Cher Visiteur";
            Textflow.setText(msg);
            ButtonMonCompte.setVisible(false);
            PariTab.setDisable(true);
        }
        else {
            String msg="Bienvenue "+this.application.getMembre().getNom()+" "+this.application.getMembre().getPrenom();
            Textflow.setText(msg);
        }
    }
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
    }    
    
    @FXML
    public void clickMonCompte () {
        if (application.getMembre() instanceof Administrateur)
            application.gotoCompteAdmin();
        else if (application.getMembre() instanceof Medecin) 
            application.gotoCompteMedecin();
        else if (application.getMembre() instanceof Fan)
            application.gotoCompteFan();
        else if (application.getMembre() instanceof Joueur)
            application.gotoCompteJoueur();
        else if (application.getMembre() instanceof ResponsableAntiDopage)
            application.gotoCompteResponsable();
        else if (application.getMembre() instanceof Arbitre)
            application.gotoCompteArbitre();
    }
    @FXML
    public void clickBack () {
        application.gotoLogin();
    }
}