/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package allwin.pkg2016.GuiForms;

import allwin.pkg2016.DAO.Implement.ClubDao;
import allwin.pkg2016.DAO.Interfaces.IClub;
import allwin.pkg2016.Entities.Administrateur;
import allwin.pkg2016.Entities.Arbitre;
import allwin.pkg2016.Entities.Club;
import allwin.pkg2016.Entities.Fan;
import allwin.pkg2016.Entities.Joueur;
import allwin.pkg2016.Entities.Medecin;
import allwin.pkg2016.Entities.Membre;
import allwin.pkg2016.Entities.ResponsableAntiDopage;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * FXML Controller class
 *
 * @author Bilel
 */
public class ClubController implements Initializable {

    Main application;
    @FXML
    private TableColumn<Club, String> ClubCol;
    @FXML
    private TableColumn<Club, String> NomCol;
    @FXML
    private TableColumn<Club, String> AdresseCol;
    @FXML
    private TableColumn<Club, String> MailCol;
    @FXML
    private Button localisation;
    @FXML
    private Button retour;
    @FXML
    
   
    private TextField rechercheTF;
    @FXML
    private TableView<Club> Table2 = null;
    @FXML
    private TableView<Club> Table1 = null;
    List<Club> clubs = new ArrayList<>();
    IClub C = new ClubDao();
    ObservableList<Club> champs = FXCollections.observableArrayList(C.DisplayAllClub());
    ObservableList<Club> champsNomClub = FXCollections.observableArrayList(C.DisplayAllClub());
    ObservableList<Membre> observableMembreList = FXCollections.observableArrayList();
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        rechercheTF.textProperty().addListener(new ChangeListener() {

            @Override
            public void changed(ObservableValue observable, Object oldValue, Object newValue) {

                filterMembreList((String) oldValue, (String) newValue);

            }

        });
        System.out.println(champs);
        System.out.println(champsNomClub);
        NomCol.setCellValueFactory(new PropertyValueFactory<>("nom"));
        AdresseCol.setCellValueFactory(new PropertyValueFactory<>("adresse"));
        MailCol.setCellValueFactory(new PropertyValueFactory<>("mail"));
        ClubCol.setCellValueFactory(new PropertyValueFactory<>("nom"));
//        ClubCol.setCellValueFactory((TableColumn.CellDataFeatures<Club, String> p) -> new SimpleStringProperty(p.getValue().getNom()));
        Table2.setItems(champs);
        Table1.setItems(champsNomClub);
        Table2.setTableMenuButtonVisible(true);
    }

    @FXML
    private void clickLocalisation(ActionEvent event) {
    }

    @FXML
    private void clickBack(ActionEvent event) {
        if (application.getMembre() instanceof Administrateur)
            application.gotoCompteAdmin();
        else if (application.getMembre() instanceof Medecin) 
            application.gotoCompteMedecin();
        else if (application.getMembre() instanceof Fan)
            application.gotoCompteFan();
        else if (application.getMembre() instanceof Joueur)
            application.gotoCompteJoueur();
        else if (application.getMembre() instanceof ResponsableAntiDopage)
            application.gotoCompteResponsable();
        else if (application.getMembre() instanceof Arbitre)
            application.gotoCompteArbitre();
    }

    public void setApp(Main application) {
        this.application = application;
    }
    private void filterMembreList(String oldValue, String newValue) {
        ObservableList<Club> filteredList = FXCollections.observableArrayList();

        if (rechercheTF == null || (newValue.length() < oldValue.length()) || newValue == null) {

            Table2.setItems(champs);

        } else {

            newValue = newValue.toUpperCase();

            for (Club clubss : Table2.getItems()) {

                String filterFirstName = clubss.getNom();

                String filterLastName = clubss.getAdresse();

                if (filterFirstName.toUpperCase().contains(newValue) || filterLastName.toUpperCase().contains(newValue)) {

                    filteredList.add(clubss);

                }

            }
            Table2.setItems(filteredList);

        }
    }
}
