/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package allwin.pkg2016.GuiForms;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author Mahmoud
 */
public class FeuilleDeMatchController implements Initializable {
    Main application;
    @FXML
    private DatePicker DateMatch;
    @FXML
    private TextField LieuTF;
    @FXML
    private TextField Joueur1TF;
    @FXML
    private TextField Joueur2TF;
    @FXML
    private ChoiceBox<String> ComboBoxResult;
    ObservableList<String> comboList = FXCollections.observableArrayList("Victoire","Défaite");
    
    @FXML
    private TextField Set11;
    @FXML
    private TextField Set12;
    @FXML
    private TextField Set21;
    @FXML
    private TextField Set22;
    @FXML
    private TextField Set31;
    @FXML
    private TextField Set32;
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        ComboBoxResult.setValue("Victoire");
        ComboBoxResult.setItems(comboList);
    }    
    
    public void setApp (Main application) {
        this.application=application;
    }
}
