/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package allwin.pkg2016.GuiForms;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

/**
 * FXML Controller class
 *
 * @author Mahmoud
 */
public class CompteJoueurController implements Initializable {
    Main application;
    
    public void setApp(Main application){
        this.application = application;
    }
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
    }    
    
    @FXML
    public void clickBack () {
        application.gotoAllWin();
    }

    @FXML
    private void modifierInfos(ActionEvent event) {
        application.gotoModifierInfos();
    }
    
    @FXML
    private void clickContacter(ActionEvent event) {
        application.gotoContacter();
    }
}
