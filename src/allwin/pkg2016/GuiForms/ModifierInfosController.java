/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package allwin.pkg2016.GuiForms;

import allwin.pkg2016.Entities.Administrateur;
import allwin.pkg2016.Entities.Arbitre;
import allwin.pkg2016.Entities.Fan;
import allwin.pkg2016.Entities.Joueur;
import allwin.pkg2016.Entities.Medecin;
import allwin.pkg2016.Entities.ResponsableAntiDopage;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;

/**
 * FXML Controller class
 *
 * @author Mahmoud
 */
public class ModifierInfosController implements Initializable {
    Main application;
    @FXML
    private TextField MailTF;
    @FXML
    private PasswordField AncienPassTF;
    @FXML
    private PasswordField NewPassTF;
    @FXML
    private PasswordField ConfPassTF;
    @FXML
    private ImageView ImageviewProfile;
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
    public void setApp(Main application) {
        this.application = application;
    }

    @FXML
    private void UsernameTF(ActionEvent event) {
    }

    @FXML
    private void clickConfirmer(ActionEvent event) {
    }

    @FXML
    private void clickImage(ActionEvent event) {
    }

    public void clickBack () {
        if (application.getMembre() instanceof Administrateur)
            application.gotoCompteAdmin();
        else if (application.getMembre() instanceof Medecin) 
            application.gotoCompteMedecin();
        else if (application.getMembre() instanceof Fan)
            application.gotoCompteFan();
        else if (application.getMembre() instanceof Joueur)
            application.gotoCompteJoueur();
        else if (application.getMembre() instanceof ResponsableAntiDopage)
            application.gotoCompteResponsable();
        else if (application.getMembre() instanceof Arbitre)
            application.gotoCompteArbitre();
    }
}
