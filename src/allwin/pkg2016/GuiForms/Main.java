/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package allwin.pkg2016.GuiForms;

import allwin.pkg2016.Entities.Competition;
import allwin.pkg2016.Entities.Membre;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.fxml.JavaFXBuilderFactory;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author Mahmoud
 */
public class Main extends Application {

    private Stage stage;
    private static Membre membre;

    public Membre getMembre() {
        return membre;
    }

    public void setMembre(Membre membre) {
        Main.membre = membre;
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        try {
            stage = primaryStage;
            gotoLogin();
            primaryStage.show();
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void main(String[] args) {
        Application.launch(Main.class, (java.lang.String[]) null);
    }

    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    private Initializable replaceSceneContent(String fxml) throws Exception {
        FXMLLoader loader = new FXMLLoader();
        InputStream in = Main.class.getResourceAsStream(fxml);
        loader.setBuilderFactory(new JavaFXBuilderFactory());
        loader.setLocation(Main.class.getResource(fxml));
        AnchorPane page;
        try {
            page = (AnchorPane) loader.load(in);
        } finally {
            in.close();
        }
        Scene scene = new Scene(page, 800, 600);
        stage.setScene(scene);
        stage.sizeToScene();
        return (Initializable) loader.getController();
    }

    public void gotoLogin() {
        try {
            AuthentificationController login = (AuthentificationController) replaceSceneContent("Authentification.fxml");
            login.setApp(this);
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void gotoAjoutJoueur() {
        try {
            AjoutJoueurController profile = (AjoutJoueurController) replaceSceneContent("AjoutJoueur.fxml");
            profile.setApp(this);
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void gotoAllWin() {
        try {
            AllWinController allwin = (AllWinController) replaceSceneContent("AllWin.fxml");
            allwin.setApp(this);
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void gotoInscription() {
        try {
            InscriptionController allwin = (InscriptionController) replaceSceneContent("Inscription.fxml");
            allwin.setApp(this);
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void gotoClub() {
        try {
            ClubController allwin = (ClubController) replaceSceneContent("Club.fxml");
            allwin.setApp(this);
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void gotoCompteAdmin() {
        try {
            CompteAdminController allwin = (CompteAdminController) replaceSceneContent("CompteAdmin.fxml");
            allwin.setApp(this);
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void gotoCompteFan() {
        try {
            CompteFanController allwin = (CompteFanController) replaceSceneContent("CompteFan.fxml");
            allwin.setApp(this);
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void gotoCompteRendu() {
        try {
            CompteRenduController allwin = (CompteRenduController) replaceSceneContent("CompteRendu.fxml");
            allwin.setApp(this);
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void gotoCompteMedecin() {
        try {
            CompteMedecinController allwin = (CompteMedecinController) replaceSceneContent("CompteMedecin.fxml");
            allwin.setApp(this);
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void gotoAjoutPersonnel() {
        try {
            AjoutPersonnelController profile = (AjoutPersonnelController) replaceSceneContent("AjoutPersonnel.fxml");
            profile.setApp(this);
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void gotoFeuilleDeMatch() {
        try {
            FeuilleDeMatchController allwin = (FeuilleDeMatchController) replaceSceneContent("FeuilleDeMatch.fxml");
            allwin.setApp(this);
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void gotoListeMembres() {
        try {
            ListeMembresController allwin = (ListeMembresController) replaceSceneContent("ListeMembres.fxml");
            allwin.setApp(this);
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void gotoCompteJoueur() {
        try {
            CompteJoueurController allwin = (CompteJoueurController) replaceSceneContent("CompteJoueur.fxml");
            allwin.setApp(this);
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void gotoCompteResponsable() {
        try {
            CompteResponsableController allwin = (CompteResponsableController) replaceSceneContent("CompteResponsable.fxml");
            allwin.setApp(this);
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void gotoSendMail() {
        try {
            SendMailController allwin = (SendMailController) replaceSceneContent("SendMail.fxml");
            allwin.setApp(this);
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void gotoListeComeptition() {
        try {
            ListeCompetitionController allwin = (ListeCompetitionController) replaceSceneContent("ListeCompetition.fxml");
            allwin.setApp(this);
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public boolean showCompetitionEditDialog(Competition competition) {
        try {
            // Load the fxml file and create a new stage for the popup dialog.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("AjouterCompetition.fxml"));
            AnchorPane page = (AnchorPane) loader.load();

            // Create the dialog Stage.
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Edit Person");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(stage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            // Set the person into the controller.
            AjouterCompetitionController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            //controller.setPerson(competition);

            // Show the dialog and wait until the user closes it
            dialogStage.showAndWait();

            return controller.isOkClicked();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
    
        public void gotoModifierInfos() {
        try {
            ModifierInfosController profile = (ModifierInfosController) replaceSceneContent("ModifierInfos.fxml");
            profile.setApp(this);
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void gotoCompteArbitre () {
        try {
            CompteArbitreController allwin = (CompteArbitreController) replaceSceneContent("CompteArbitre.fxml");
            allwin.setApp(this);
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void gotoContacter () {
        try {
            SendMailController allwin = (SendMailController) replaceSceneContent("SendMail.fxml");
            allwin.setApp(this);
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void gotoListeCompetition () {
        try {
            ListeCompetitionController allwin = (ListeCompetitionController) replaceSceneContent("ListeCompetition.fxml");
            allwin.setApp(this);
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
